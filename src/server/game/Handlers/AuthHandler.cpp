/*
 * Copyright (C) 2008-2013 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Opcodes.h"
#include "WorldSession.h"
#include "WorldPacket.h"

void WorldSession::SendAuthResponse(uint8 code, bool queued, uint32 queuePos)
{
    size_t allowedRacesCount = sObjectMgr->GetAllowedRacesSize();
    size_t allowedClassesCount = sObjectMgr->GetAllowedClassesSize();

    size_t reservedSize = 10 + (queued ? 4 : 0) + allowedRacesCount * 2 + 11 + allowedClassesCount * 2 + 5;
    WorldPacket packet(SMSG_AUTH_RESPONSE, reservedSize);

    packet.WriteBit(1);                         // hasAccountData
    packet.WriteBits(allowedClassesCount, 25);  // classes
    packet.WriteBit(0);                         // unk
    packet.WriteBit(0);                         // unk
    packet.WriteBits(0, 22);                    // hasCharacterTemplate
    packet.WriteBits(allowedRacesCount, 25);    // races
    packet.WriteBit(queued);                    // isInQueue

    if (queued)
    {
        packet.WriteBit(0);                     // unk
        packet.FlushBits();

        packet << queuePos;                     // queuePosition
    }
    else
        packet.FlushBits();

    ObjectMgr::AllowedRacesMap const& allowedRaces = sObjectMgr->GetAllowedRaces();
    for (ObjectMgr::AllowedRacesMap_ConstItr itr = allowedRaces.begin(); itr != allowedRaces.end(); ++itr)
    {
        packet << uint8(itr->second);           // expansion
        packet << uint8(itr->first);            // race
    }

    packet << uint32(0);
    packet << uint32(0);
    packet << uint8(0);
    packet << uint8(sWorld->getIntConfig(CONFIG_EXPANSION)); // expansion
    packet << uint8(sWorld->getIntConfig(CONFIG_EXPANSION)); // expansion

    ObjectMgr::AllowedClassesMap const& allowedClasses = sObjectMgr->GetAllowedClasses();
    for (ObjectMgr::AllowedClassesMap_ConstItr itr = allowedClasses.begin(); itr != allowedClasses.end(); ++itr)
    {
        packet << uint8(itr->first);            // class
        packet << uint8(itr->second);           // expansion
    }
    packet << uint32(0);                        // unk
    packet << uint8(code);                      // errorCode

    SendPacket(&packet);
}

void WorldSession::SendClientCacheVersion(uint32 version)
{
    WorldPacket data(SMSG_CLIENTCACHE_VERSION, 4);
    data << uint32(version);
    SendPacket(&data);
}
