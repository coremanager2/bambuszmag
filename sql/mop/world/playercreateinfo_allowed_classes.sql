DROP TABLE IF EXISTS playercreateinfo_allowed_classes;
CREATE TABLE playercreateinfo_allowed_classes (
    `class` TINYINT(1) UNSIGNED NOT NULL,
    expansion TINYINT(1) UNSIGNED NOT NULL,
    enabled TINYINT(1) UNSIGNED NOT NULL,
    PRIMARY KEY(`class`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO playercreateinfo_allowed_classes (`class`, expansion, enabled) VALUES
(1, 0, 1),  -- warrior
(2, 0, 1),  -- paladin
(3, 0, 1),  -- hunter
(4, 0, 1),  -- rogue
(5, 0, 1),  -- priest
(6, 2, 1),  -- death knight
(7, 0, 1),  -- shaman
(8, 0, 1),  -- mage
(9, 0, 1),  -- warlock
(10, 4, 1), -- monk
(11, 0, 1); -- druid
