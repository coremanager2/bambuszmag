DROP TABLE IF EXISTS playercreateinfo_allowed_races;
CREATE TABLE playercreateinfo_allowed_races (
    race TINYINT(1) UNSIGNED NOT NULL,
    expansion TINYINT(1) UNSIGNED NOT NULL,
    enabled TINYINT(1) UNSIGNED NOT NULL,
    PRIMARY KEY(race)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO playercreateinfo_allowed_races (race, expansion, enabled) VALUES
(1, 0, 1),  -- human
(2, 0, 1),  -- orc
(3, 0, 1),  -- dwarf
(4, 0, 1),  -- nifht elf
(5, 0, 1),  -- undead
(6, 0, 1),  -- tauren
(7, 0, 1),  -- gnome
(8, 0, 1),  -- troll
(9, 3, 1),  -- goblin
(10, 1, 1), -- blood elf
(11, 1, 1), -- draenei
(22, 3, 1), -- worgen
(24, 4, 1), -- pandaren neutral
(25, 4, 1), -- pandaren alliance
(26, 4, 1); -- pandaren horde
